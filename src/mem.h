#ifndef DBMS_MEM_H
#define DBMS_MEM_H
#include "dbms.h"
#include "dispatcher.h"

void freeTypes(Students students, Courses courses, Lecturers lecturers);
void freeCommand(Commands *commands, int index);
void freeValues(CommonValues *values, int index);

#endif //DBMS_MEM_H
