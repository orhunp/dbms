#ifndef DBMS_HANDLER_H
#define DBMS_HANDLER_H
#include "helper.h"
#include "dbms.h"

Students add_student_to_course(Students *students, Courses *courses, char *args[]);
Lecturers assign_course_to_lecturer(Lecturers *lecturers, Courses *courses, char *args[]);
Students remove_student_from_course(Students *students, Courses *courses, char *args[]);
void print_student_info(FILE *file, Students students);
void print_lecturer_info(FILE *file, Lecturers lecturers);
void print_course_info(FILE *file, Courses courses);
void print_student_course_info(FILE *file, Students students, int number);
void print_lecturer_course_info(FILE *file, Lecturers lecturers, char *number);
void print_course_participants(FILE *file, Courses courses, Lecturers lecturers, Students students, char *code);
void print_student_credits(FILE *file, Students students, int credit_threshold);

#endif //DBMS_HANDLER_H
