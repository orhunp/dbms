#ifndef DBMS_HELPER_H
#define DBMS_HELPER_H
#include "dbms.h"

int get_student_index(Students students, int number);
int get_course_index(Courses courses, char *code);
int get_lecturer_index(Lecturers lecturers, char *number);
int get_student_credits(Student student);
int get_student_course(Student student, char *code);
int get_course_lecturer(Lecturers lecturers, char *code);

#endif //DBMS_HELPER_H
