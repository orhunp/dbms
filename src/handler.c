#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "handler.h"

Students add_student_to_course(Students *students, Courses *courses, char *args[]) {
    int student_index = get_student_index(
            (*students), (int) strtol(args[0], NULL, 10));
    if (student_index == ERR_NOT_FOUND) {
        fprintf(stderr, "ERR: Student not found: %s\n", args[0]);
        return (*students);
    }
    int student_course_count = (*students).inner[student_index].course_index;
    if (student_course_count >= STUDENT_MAX_COURSES) {
        fprintf(stderr, "ERR: Student reached the maximum number of courses: %d/%d\n",
                student_course_count, STUDENT_MAX_COURSES);
        return (*students);
    }
    int student_credits = get_student_credits((*students).inner[student_index]);
    if (student_credits >= STUDENT_MAX_CREDITS) {
        fprintf(stderr, "ERR: Student reached the maximum number of credits: %d/%d\n",
                student_credits, STUDENT_MAX_CREDITS);
        return (*students);
    }
    int course_index = get_course_index((*courses), args[1]);
    if (course_index == ERR_NOT_FOUND) {
        fprintf(stderr, "ERR: Course not found: %s\n", args[1]);
        return (*students);
    }
    if (get_student_course(students->inner[student_index], courses->inner[course_index].code) != ERR_NOT_FOUND) {
        fprintf(stderr, "ERR: Student is already in this course.\n");
        return (*students);
    }
    Course course = (*courses).inner[course_index];
    (*students).inner[student_index].courses[(*students).inner[student_index].course_index] = course;
    (*students).inner[student_index].course_index++;
    return (*students);
}

Lecturers assign_course_to_lecturer(Lecturers *lecturers, Courses *courses, char *args[]) {
    int lecturer_index = get_lecturer_index((*lecturers), args[0]);
    if (lecturer_index == ERR_NOT_FOUND) {
        fprintf(stderr, "ERR: Lecturer not found: %s\n", args[0]);
        return (*lecturers);
    }
    int course_index = get_course_index((*courses), args[1]);
    if (course_index == ERR_NOT_FOUND) {
        fprintf(stderr, "ERR: Course not found: %s\n", args[1]);
        return (*lecturers);
    }
    int lecturer_course_count = (*lecturers).inner[lecturer_index].course_index;
    if (lecturer_course_count >= LECTURER_MAX_COURSES) {
        fprintf(stderr, "ERR: Lecturer reached the maximum number of courses: %d/%d\n",
                lecturer_course_count, LECTURER_MAX_COURSES);
        return (*lecturers);
    }
    if (get_course_lecturer(*lecturers, args[1]) != ERR_NOT_FOUND) {
        fprintf(stderr, "This course is already taught by a lecturer.\n");
        return (*lecturers);
    }
    Course course = (*courses).inner[course_index];
    (*lecturers).inner[lecturer_index].courses[(*lecturers).inner[lecturer_index].course_index] = course;
    (*lecturers).inner[lecturer_index].course_index++;
    return (*lecturers);
}

Students remove_student_from_course(Students *students, Courses *courses, char *args[]) {
    int student_index = get_student_index(
            (*students), (int) strtol(args[0], NULL, 10));
    if (student_index == ERR_NOT_FOUND) {
        fprintf(stderr, "ERR: Student not found: %s\n", args[0]);
        return (*students);
    }
    if (get_course_index((*courses), args[1]) == ERR_NOT_FOUND) {
        fprintf(stderr, "ERR: Course not found: %s\n", args[1]);
        return (*students);
    }
    int student_course_index =
            get_student_course((*students).inner[student_index], args[1]);
    if (student_course_index == ERR_NOT_FOUND) {
        fprintf(stderr, "ERR: Student is not participated the specified course.\n");
        return (*students);
    }
    memmove(&(*students).inner[student_index].courses[student_course_index],
            &(*students).inner[student_index].courses[student_course_index + 1],
            ((STUDENT_MAX_COURSES-1)-student_index) * sizeof(Course));
    (*students).inner[student_index].course_index--;
    return (*students);
}

void print_student_info(FILE *file, Students students) {
    if (students.size == 0) {
        fprintf(file, "EMPTY\n");
    }
    for (int i = 0; i < students.size; i++) {
        fprintf(file, "%-2d %-7s %-7s\n", students.inner[i].number,
                students.inner[i].name, students.inner[i].surname);
    }
}

void print_lecturer_info(FILE *file, Lecturers lecturers) {
    if (lecturers.size == 0) {
        fprintf(file, "EMPTY\n");
    }
    for (int i = 0; i < lecturers.size; i++) {
        fprintf(file, "%-6s %-10s %-10s\n", lecturers.inner[i].number,
                lecturers.inner[i].name, lecturers.inner[i].surname);
    }
}

void print_course_info(FILE *file, Courses courses) {
    if (courses.size == 0) {
        fprintf(file, "EMPTY\n");
    }
    for (int i = 0; i < courses.size; i++) {
        fprintf(file, "%-7s %-20s %-2d\n", courses.inner[i].code,
                courses.inner[i].name, courses.inner[i].credit);
    }
}

void print_student_course_info(FILE *file, Students students, int number) {
    int student_index = get_student_index(students, number);
    if (student_index == ERR_NOT_FOUND) {
        fprintf(file, "EMPTY\n");
        return;
    } else if (students.inner[student_index].course_index == 0) {
        fprintf(file, "EMPTY\n");
        return;
    }
    fprintf(file, "Student: %d - %s %s\nParticipating Courses: %d\n",
            students.inner[student_index].number, students.inner[student_index].name,
            students.inner[student_index].surname, students.inner[student_index].course_index);
    for (int i = 0; i < students.inner[student_index].course_index; i++) {
        fprintf(file, "%s %s %d\n",
                students.inner[student_index].courses[i].code,
                students.inner[student_index].courses[i].name,
                students.inner[student_index].courses[i].credit);
    }
    fprintf(file, "Achieved Credits: %d\n", get_student_credits(students.inner[student_index]));
}

void print_lecturer_course_info(FILE *file, Lecturers lecturers, char *number) {
    int lecturer_index = get_lecturer_index(lecturers, number);
    if (lecturer_index == ERR_NOT_FOUND) {
        fprintf(file, "EMPTY\n");
        return;
    } else if (lecturers.inner[lecturer_index].course_index == 0) {
        fprintf(file, "EMPTY\n");
        return;
    }
    fprintf(file, "Lecturer: %s - %s %s\nThe number of courses taught: %d\n",
            lecturers.inner[lecturer_index].number, lecturers.inner[lecturer_index].name,
            lecturers.inner[lecturer_index].surname, lecturers.inner[lecturer_index].course_index);
    for (int i = 0; i < lecturers.inner[lecturer_index].course_index; i++) {
        fprintf(file, "%s %s %d\n",
                lecturers.inner[lecturer_index].courses[i].code,
                lecturers.inner[lecturer_index].courses[i].name,
                lecturers.inner[lecturer_index].courses[i].credit);
    }
}

void print_course_participants(FILE *file, Courses courses, Lecturers lecturers, Students students, char *code) {
    int course_index = get_course_index(courses, code);
    if (course_index == ERR_NOT_FOUND) {
        fprintf(file, "EMPTY\n");
        return;
    }
    Students participants = {0};
    for (int i = 0; i < students.size; i++) {
        int student_course_index = get_student_course(students.inner[i], code);
        if (student_course_index != ERR_NOT_FOUND)
            participants.inner[participants.size++] = students.inner[i];
    }
    if (participants.size == 0) {
        fprintf(file, "EMPTY\n");
        return;
    }
    int course_lecturer_index = get_course_lecturer(lecturers, code);
    if (course_lecturer_index == ERR_NOT_FOUND) {
        fprintf(file, "EMPTY\n");
        return;
    }
    fprintf(file,
            "Course: %s %s - %d\n"
            "The Lecturer: %s - %s %s\n"
            "The number of participating students: %d\n",
            courses.inner[course_index].code,
            courses.inner[course_index].name,
            courses.inner[course_index].credit,
            lecturers.inner[course_lecturer_index].number,
            lecturers.inner[course_lecturer_index].name,
            lecturers.inner[course_lecturer_index].surname,
            participants.size);
    for (int i = 0; i < participants.size; i++) {
        fprintf(file, "%d %-2d %-7s %-7s\n", i + 1, participants.inner[i].number,
                participants.inner[i].name, participants.inner[i].surname);
    }
}

void print_student_credits(FILE *file, Students students, int credit_threshold) {
    if (students.size == 0) {
        fprintf(file, "EMPTY\n");
        return;
    }
    Students target_students = {0};
    for (int i = 0; i < students.size; i++) {
        if (get_student_credits(students.inner[i]) < credit_threshold &&
            get_student_credits(students.inner[i]) != 0)
            target_students.inner[target_students.size++] = students.inner[i];
    }
    if (target_students.size == 0) {
        fprintf(file, "EMPTY\n");
        return;
    }
    for (int i = 0; i < target_students.size; i++) {
        fprintf(file, "%-2d %-7s %-7s\n", target_students.inner[i].number,
                target_students.inner[i].name, target_students.inner[i].surname);
    }
}
