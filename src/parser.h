#ifndef DBMS_PARSER_H
#define DBMS_PARSER_H
#include "dbms.h"

#define FILE_LINE_LENGTH 256

CommonValues parseCommonValues(char *file_name);
void parseStudents(Students *students, char *file_name);
void parseLecturers(Lecturers *lecturers, char *file_name);
void parseCourses(Courses *courses, char *file_name);

#endif //DBMS_PARSER_H
