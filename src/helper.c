#include <string.h>
#include <stdlib.h>
#include "helper.h"

int get_student_index(Students students, int number) {
    for (int i = 0; i < students.size; i++) {
        if (students.inner[i].number == number) {
            return i;
        }
    }
    return ERR_NOT_FOUND;
}

int get_course_index(Courses courses, char *code) {
    for (int i = 0; i < courses.size; i++) {
        if (!strncmp(code, courses.inner[i].code, strlen(courses.inner[i].code))) {
            return i;
        }
    }
    return ERR_NOT_FOUND;
}

int get_lecturer_index(Lecturers lecturers, char *number) {
    for (int i = 0; i < lecturers.size; i++) {
        if (!strncmp(number, lecturers.inner[i].number, strlen(lecturers.inner[i].number))) {
            return i;
        }
    }
    return ERR_NOT_FOUND;
}

int get_student_credits(Student student) {
    int credits = 0;
    for (int i = 0; i < student.course_index; i++) {
        credits += student.courses[i].credit;
    }
    return credits;
}

int get_student_course(Student student, char *code) {
    for (int i = 0; i < student.course_index; i++) {
        if (!strncmp(code, student.courses[i].code, strlen(student.courses[i].code))) {
            return i;
        }
    }
    return ERR_NOT_FOUND;
}

int get_course_lecturer(Lecturers lecturers, char *code) {
    for (int x = 0; x < lecturers.size; x++) {
        for (int y = 0; y < lecturers.inner[x].course_index; y++) {
            if (!strncmp(lecturers.inner[x].courses[y].code, code, strlen(code))) {
                return x;
            }
        }
    }
    return ERR_NOT_FOUND;
}
