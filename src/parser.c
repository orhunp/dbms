#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "parser.h"
#include "mem.h"

CommonValues parseCommonValues(char *file_name) {
    CommonValues values;
    FILE *file = fopen(file_name, "r");
    if (file == NULL) {
        fprintf(stderr, "ERR: File not found: \"%s\"", file_name);
        exit(EXIT_FAILURE);
    }
    char line[FILE_LINE_LENGTH], val1[FILE_LINE_LENGTH],
        val2[FILE_LINE_LENGTH], val3[FILE_LINE_LENGTH];
    for (int i = 0; fgets(line, FILE_LINE_LENGTH, file); i++) {
        if (i == 0) {
            strtok(line, ":");
            values.size = (int) strtol(strtok(NULL, ":"), NULL, 10);
        } else {
            sscanf(line, "%256s %256s %[^\n]\n", val1, val2, val3);
            values.inner[i-1].val1 = strdup(val1);
            values.inner[i-1].val2 = strdup(val2);
            values.inner[i-1].val3 = strdup(val3);
        }
    }
    fclose(file);
    return values;
}

void parseStudents(Students *students, char *file_name) {
    CommonValues values = parseCommonValues(file_name);
    students->size = values.size;
    for (int i = 0; i < values.size; i++) {
        students->inner[i].number = (int) strtol(values.inner[i].val1, NULL, 10);
        students->inner[i].name = strdup(values.inner[i].val2);
        students->inner[i].surname = strdup(values.inner[i].val3);
        freeValues(&values, i);
    }
}

void parseLecturers(Lecturers *lecturers, char *file_name) {
    CommonValues values = parseCommonValues(file_name);
    lecturers->size = values.size;
    for (int i = 0; i < values.size; i++) {
        lecturers->inner[i].number = strdup(values.inner[i].val1);
        lecturers->inner[i].name = strdup(values.inner[i].val2);
        lecturers->inner[i].surname = strdup(values.inner[i].val3);
        freeValues(&values, i);
    }
}

void parseCourses(Courses *courses, char *file_name) {
    CommonValues values = parseCommonValues(file_name);
    courses->size = values.size;
    for (int i = 0; i < values.size; i++) {
        courses->inner[i].code = strdup(values.inner[i].val1);
        courses->inner[i].name = strdup(values.inner[i].val2);
        courses->inner[i].credit = (int) strtol(values.inner[i].val3, NULL, 10);
        freeValues(&values, i);
    }
}
