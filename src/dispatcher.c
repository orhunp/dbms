#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "dispatcher.h"
#include "parser.h"

Commands dispatch_commands(char *command_file) {
    Commands commands;
    FILE *file = fopen(command_file, "r");
    if (file == NULL) {
        fprintf(stderr, "ERR: File not found: \"%s\"", command_file);
        exit(EXIT_FAILURE);
    }
    char line[FILE_LINE_LENGTH];
    for (int i = 0; fgets(line, FILE_LINE_LENGTH, file); i++) {
        line[strcspn(line, "\n")] = 0;
        Command command = {.raw = strdup(line)};
        char *token;
        char *remaining = line;
        for (int x = 0; (token = strtok_r(remaining, " ", &remaining)); x++) {
            if (x == 0) {
                command.main = strdup(token);
            } else {
                command.args[x-1] = strdup(token);
            }
        }
        commands.inner[i] = command;
        commands.size++;
    }
    fclose(file);
    return commands;
}
