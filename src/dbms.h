#ifndef DBMS_DBMS_H
#define DBMS_DBMS_H

#define REQUIRED_ARG_COUNT 6
#define STUDENTS_FILE "students.txt"
#define LECTURERS_FILE "lecturers.txt"
#define COURSES_FILE "courses.txt"
#define COMMANDS_FILE "commands.txt"
#define MAX_MEMBERS 512
#define STUDENT_MAX_COURSES 8
#define STUDENT_MAX_CREDITS 22
#define LECTURER_MAX_COURSES 5

#define ERR_NOT_FOUND -1

typedef struct Course_t {
    char *code;
    char *name;
    int credit;
} Course;

typedef struct Courses_t {
    Course inner[MAX_MEMBERS];
    int size;
} Courses;

typedef struct Student_t {
    int number;
    char *name;
    char *surname;
    Course courses[STUDENT_MAX_COURSES];
    int course_index;
} Student;

typedef struct Students_t {
    Student inner[MAX_MEMBERS];
    int size;
} Students;

typedef struct Lecturer_t {
    char *number;
    char *name;
    char *surname;
    Course courses[LECTURER_MAX_COURSES];
    int course_index;
} Lecturer;

typedef struct Lecturers_t {
    Lecturer inner[MAX_MEMBERS];
    int size;
} Lecturers;

typedef struct CommonValue_t {
    char *val1;
    char *val2;
    char *val3;
} CommonValue;

typedef struct CommonValues_t {
    CommonValue inner[MAX_MEMBERS];
    int size;
} CommonValues;

#endif //DBMS_DBMS_H
