#include <stdlib.h>
#include "mem.h"

void freeTypes(Students students, Courses courses, Lecturers lecturers) {
    for (int x = 0; x < students.size; x++) {
        free(students.inner[x].name);
        free(students.inner[x].surname);
    }
    for (int x = 0; x < courses.size; x++) {
        free(courses.inner[x].code);
        free(courses.inner[x].name);
    }
    for (int x = 0; x < lecturers.size; x++) {
        free(lecturers.inner[x].number);
        free(lecturers.inner[x].name);
        free(lecturers.inner[x].surname);
    }
}

void freeCommand(Commands *commands, int index) {
    free((*commands).inner[index].main);
    free((*commands).inner[index].raw);
    for (int x = 0; x < MAX_ARGS; x++) {
        free((*commands).inner[index].args[x]);
    }
}

void freeValues(CommonValues *values, int index) {
    free((*values).inner[index].val1);
    free((*values).inner[index].val2);
    free((*values).inner[index].val3);
}
