#ifndef DBMS_DISPATCHER_H
#define DBMS_DISPATCHER_H

#define MAX_COMMANDS 256
#define MAX_ARGS 16

typedef struct Command_t {
    char *main;
    char *args[MAX_ARGS];
    char *raw;
} Command;

typedef struct Commands_t {
    Command inner[MAX_COMMANDS];
    int size;
} Commands;

Commands dispatch_commands(char *command_file);

#endif //DBMS_DISPATCHER_H
