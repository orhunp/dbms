#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "dbms.h"
#include "parser.h"
#include "dispatcher.h"
#include "handler.h"
#include "mem.h"

static Students students;
static Lecturers lecturers;
static Courses courses;
static char *command_file;
static FILE *output_file;

int main(int argc, char *argv[]) {
    if (argc != REQUIRED_ARG_COUNT) {
        fprintf(stderr, "ERR: You sure that you provided enough arguments?");
        return EXIT_FAILURE;
    }
    for (int i = 1; i < REQUIRED_ARG_COUNT; i++) {
        if (!strncmp(argv[i], STUDENTS_FILE, strlen(STUDENTS_FILE))) {
            parseStudents(&students, argv[i]);
        } else if (!strncmp(argv[i], LECTURERS_FILE, strlen(LECTURERS_FILE))) {
            parseLecturers(&lecturers, argv[i]);
        } else if (!strncmp(argv[i], COURSES_FILE, strlen(COURSES_FILE))) {
            parseCourses(&courses, argv[i]);
        } else if (!strncmp(argv[i], COMMANDS_FILE, strlen(COMMANDS_FILE))) {
            command_file = argv[i];
        } else {
            output_file = fopen(argv[i], "w+");
        }
    }
    Commands commands = dispatch_commands(command_file);
    for (int i = 0; i < commands.size; i++) {
        fprintf(output_file,"Command:\n%s\nResult:\n", commands.inner[i].raw);
        if (!strcmp(commands.inner[i].main, "ADD")) {
            students = add_student_to_course(&students, &courses, commands.inner[i].args);
        } else if (!strcmp(commands.inner[i].main, "ASSIGN")) {
            lecturers = assign_course_to_lecturer(&lecturers, &courses, commands.inner[i].args);
        } else if (!strcmp(commands.inner[i].main, "DROP")) {
            students = remove_student_from_course(&students, &courses, commands.inner[i].args);
        } else if (!strcmp(commands.inner[i].main, "LIST1")) {
            print_student_info(output_file, students);
        } else if (!strcmp(commands.inner[i].main, "LIST2")) {
            print_lecturer_info(output_file, lecturers);
        } else if (!strcmp(commands.inner[i].main, "LIST3")) {
            print_course_info(output_file, courses);
        } else if (!strcmp(commands.inner[i].main, "LIST4")) {
            print_student_course_info(output_file, students,(int) strtol(
                    commands.inner[i].args[0], NULL, 16));
        } else if (!strcmp(commands.inner[i].main, "LIST5")) {
            print_lecturer_course_info(output_file, lecturers, commands.inner[i].args[0]);
        } else if (!strcmp(commands.inner[i].main, "LIST6")) {
            print_course_participants(output_file, courses, lecturers, students, commands.inner[i].args[0]);
        } else if (!strcmp(commands.inner[i].main, "LIST7")) {
            print_student_credits(output_file, students, (int) strtol(
                    commands.inner[i].args[0], NULL, 16));
        }
        fprintf(output_file, "\n");
        freeCommand(&commands, i);
    }
    freeTypes(students, courses, lecturers);
    fclose(output_file);
    return EXIT_SUCCESS;
}
